(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(wombat))
 '(custom-safe-themes
   '("3a2e0c5597f6d74d99daa2b5bbbc2a653d02d6b88fcd73d3c84ebf25cde37b3f" default))
 '(delete-selection-mode t)
 '(fido-mode t)
 '(global-hl-line-mode t)
 '(global-visual-line-mode t)
 '(ido-enable-flex-matching t)
 '(ido-mode 'both nil (ido))
 '(inhibit-startup-screen t)
 '(initial-major-mode 'text-mode)
 '(initial-scratch-message nil)
 '(menu-bar-mode nil)
 '(org-startup-with-inline-images t)
 '(package-selected-packages
   '(company gotham-theme ox-hugo ob-powershell org-roam org-download))
 '(scroll-bar-mode nil)
 '(tab-bar-history-mode t)
 '(tab-bar-show nil)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:inherit highlight :extend t :underline nil)))))

;; Auto-install all packages in package-selected-packages 
;; https://stackoverflow.com/a/39891192/11107264
(package-initialize)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(unless package-archive-contents
  (package-refresh-contents))
(package-install-selected-packages)

;; Not displaying line numbers in certain modes
;; https://www.emacswiki.org/emacs/LineNumbers
(require 'display-line-numbers)

(defcustom display-line-numbers-exempt-modes
  '(org-mode vterm-mode eshell-mode shell-mode term-mode ansi-term-mode doc-view-mode image-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))

(global-display-line-numbers-mode)


;; Tabbar history keybindings
(global-set-key (kbd "M-[") 'tab-bar-history-back)
(global-set-key (kbd "M-]") 'tab-bar-history-forward)

;; Window switching keybindings
;; Rebinding C-x o to M-o to switch windows
(global-set-key (kbd "M-o") 'other-window)
;; Use windmove default keybindings to move around (shift + arrow keys)
(windmove-default-keybindings)

;; Activate org-download
(require 'org-download)
;; Responsively display org-mode inline images everytime window is resized
;; keep it width 80 or below (with a two-character wide gap)
(defun org-image-resize (frame)
  (when (derived-mode-p 'org-mode)
      (if (< (window-total-width) 80)
	  (setq org-image-actual-width (- (window-pixel-width) (* (window-font-width) 2) ))
	(setq org-image-actual-width (* 78 (window-font-width)))) ;; Subtracted 2 for fringe
      (sit-for 0.2)
      (org-redisplay-inline-images)))
(add-hook 'window-size-change-functions 'org-image-resize)

;; Uses the same key for pasting image (org-download-clipboard) or text (yank) depends on the MIME type
(defun org-paste ()
  (interactive)
  (if (string-match "image" (shell-command-to-string "wl-paste --list-types"))
      (org-download-clipboard)
    (yank)))

;; Rebinding the C-y (yank) key to the function above 
(defun my-org-mode-keys ()
  "My keys for org-mode"
  (interactive)
  (local-set-key (kbd "C-y") 'org-paste))
(add-hook 'org-mode-hook 'my-org-mode-keys)

;; ox-hugo
(with-eval-after-load 'ox
  (require 'ox-hugo))

;; Org-roam
(require 'org-roam)
;; TODO: functions for create and switching vault
;; Check if the variable is set or not to determine first time running 
;; Create Vault:

;; Create ~/org-roam if not exist
(setq org-roam-directory (file-truename "~/org-roam"))
(unless (file-exists-p org-roam-directory)
  (make-directory org-roam-directory))
(org-roam-db-autosync-mode)

(global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle) 
(global-set-key (kbd "C-c n f") 'org-roam-node-find) ;; open node
(global-set-key (kbd "C-c n i") 'org-roam-node-insert) ;; create new note and link it at the cursor 
(global-set-key (kbd "C-c n c") 'org-roam-capture) ;; create new note
(global-set-key (kbd "C-c <tab>") 'completion-at-point) ;; e.g. type "s" and C-c <tab> will insert the entire link of "[id:<id>][second]" in place

;; Quick and dirty search for org-roam nodes because online solutions didn't work
(defun org-roam-search ()
  (interactive)
  (find-grep (format "find %s -type f -iname '*.org' -exec grep --color=auto -nH --null -i -e %s \{\} +" org-roam-directory (grep-read-regexp)) ))
(global-set-key (kbd "C-c s") 'org-roam-search)
